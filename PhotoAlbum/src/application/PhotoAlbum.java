package application;

import java.io.IOException;

import view.LoginController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class PhotoAlbum extends Application {
	@Override
	public void start(Stage primaryStage) throws IOException, ClassNotFoundException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/Login.fxml"));
		Pane myPane = (Pane) loader.load();
		LoginController controller = (LoginController) loader.getController();
		controller.setPrevStage(primaryStage);
		controller.start(primaryStage);		
		Scene myScene = new Scene(myPane);
		primaryStage.setScene(myScene);
		primaryStage.setResizable(false);
		primaryStage.show();
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	          public void handle(WindowEvent we) {
	          }
	      });     
	}

	public static void main(String[] args) {
		launch(args);
	}
}
