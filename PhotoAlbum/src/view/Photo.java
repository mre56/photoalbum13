package view;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import javafx.util.Pair;

public class Photo implements java.io.Serializable {

	private static final long serialVersionUID = 9131501111034675963L;
	String path;
	Calendar cal = Calendar.getInstance();
	ArrayList<Pair<String, String>> tags = new ArrayList<Pair<String, String>>();
	String caption = "no caption";

	public Photo(String path) {
		this.path = path;
		setDate();
	}
	
	public void addTag(Pair<String, String> pair) {
		tags.add(pair);
	}

	public void setCaption(String caption) {
		this.caption = caption;
	}
	public void setDate() {
        File file = new File(path);
		Long dateLong = file.lastModified();
		cal.setTimeInMillis(dateLong);
	    cal.set(Calendar.MILLISECOND,0);
	}
	public String toStringtags(Photo p) {
		String tagsString = "";
    	for(Pair<String, String> pr: p.tags) {
    		tagsString += pr.getKey().toString() + ": " + pr.getValue().toString() + '\n';
    	}
    	return tagsString;
	}
	public boolean equals(Object o) {	
		return this.path == ((Photo) o).path;
	}
}
