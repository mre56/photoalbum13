package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class AdminController {
    
    Stage prevStage;
    
    @FXML Button createButton;
    @FXML Button deleteButton;
    @FXML ListView<String> usernameListView;
    
    ArrayList<User> userList = new ArrayList<User>();
    ArrayList<String> usernameList = new ArrayList<String>();
    ObservableList<String> ouserList = FXCollections.observableArrayList();

	private ObjectInputStream obj_in;

    public void setPrevStage(Stage stage){
        this.prevStage = stage;
    }
    
    public void start(Stage mainStage) throws FileNotFoundException, IOException, ClassNotFoundException {
		Object loader;

        File usernameFile = new File("userList.ser");
        if(!usernameFile.exists()) {
        	usernameFile.createNewFile();
        } else {
	        //Read from disk using FileInputStream
	        FileInputStream f_in = new FileInputStream("userList.ser");
	        obj_in = new ObjectInputStream(f_in);
	        //Read an object
	        Object obj = obj_in.readObject();
	        if (obj instanceof ArrayList)
	        {
	            //Cast object to ArrayList
	            userList = (ArrayList<User>) obj;
	        }
	        for(User u: userList) {
	        	usernameList.add(u.getUserName());
	        }
        }
        java.util.Collections.sort(usernameList, String.CASE_INSENSITIVE_ORDER);
        ouserList = FXCollections.observableArrayList(usernameList);
        usernameListView.setItems(ouserList);
    }
    
    public void handleCreateButton(ActionEvent e) throws FileNotFoundException, IOException {
        //Create Pane
        Dialog<ButtonType> createUserDialog = new Dialog<>();
        createUserDialog.setTitle("Admin Controll: Create");
        createUserDialog.setHeaderText("Create New Login User");
        
        //Create Buttons
        ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
        createUserDialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
        
        //Create Field and label
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        TextField username = new TextField();
        username.setPromptText("Username");
        grid.add(new Label("Username:"), 0, 0);
        grid.add(username, 1, 0);
        
        //Enalbe/disable Button
        Node okButton = createUserDialog.getDialogPane().lookupButton(loginButtonType);
        okButton.setDisable(true);
        
        username.textProperty().addListener((observable, oldValue, newValue) -> {
            okButton.setDisable(true);
            boolean validUsername = true;
            for (String a:usernameList)
            {
                if (a.equalsIgnoreCase(newValue) || newValue.equalsIgnoreCase("admin"))
                    validUsername = false;
            }
            if (validUsername)
                okButton.setDisable(false);
        });
        
        createUserDialog.getDialogPane().setContent(grid);
        
        Platform.runLater(() -> username.requestFocus());
        
        Optional<ButtonType> result = createUserDialog.showAndWait();
        if (result.isPresent() && !username.getText().equals("") && result.get() == loginButtonType) {
        	User user = new User(username.getText());
        	userList.add(user);
            usernameList.add(username.getText());
            //Write to disk with FileOutputStream
            FileOutputStream f_out = new FileOutputStream("userList.ser");
            //Write object with ObjectOutputStream
            ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
            //Write object out to disk
            java.util.Collections.sort(usernameList, String.CASE_INSENSITIVE_ORDER);
            obj_out.writeObject(userList);
            ouserList = FXCollections.observableArrayList(usernameList);
            
            usernameListView.setItems(ouserList);
        }
    }
    
    public void handleDeleteButton(ActionEvent e) throws FileNotFoundException, IOException, ClassNotFoundException{
        if (!usernameListView.getSelectionModel().isEmpty())
        {
            //Delete Album(or anything) Dialog

            Alert deleteAlert = new Alert(AlertType.CONFIRMATION);
            deleteAlert .setTitle("Confirmation Dialog");
            deleteAlert .setHeaderText("Delete User "+usernameListView.getSelectionModel().getSelectedItem());
            deleteAlert .setContentText("Are you sure?");

            Optional<ButtonType> result = deleteAlert.showAndWait();
            if (result.get() == ButtonType.OK){
                String Removed = usernameList.remove(usernameListView.getSelectionModel().getSelectedIndex());
                int i = 0;
                for(User u: userList) {
                    if(Removed.equalsIgnoreCase(u.getUserName())) {
            		userList.remove(i);
            		break;
                    }
                    i++;
                }
                //Write to disk with FileOutputStream
                FileOutputStream f_out = new FileOutputStream("userList.ser");
                //Write object with ObjectOutputStream
                ObjectOutputStream obj_out = new ObjectOutputStream(f_out);
                //Write object out to disk
                obj_out.writeObject(userList);
                java.util.Collections.sort(usernameList, String.CASE_INSENSITIVE_ORDER);

                ouserList = FXCollections.observableArrayList(usernameList);
                usernameListView.setItems(ouserList);
            } 
        }
    }
}