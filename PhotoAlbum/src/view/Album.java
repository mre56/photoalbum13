package view;

import java.util.ArrayList;
import java.util.Calendar;

public class Album implements  java.io.Serializable {
	private static final long serialVersionUID = 7303624005012605889L;
	ArrayList<Photo> photos = new ArrayList<Photo>();
	int numOfPhotos = 0;
	String albumName = "";
	Calendar startDate = Calendar.getInstance();
	Calendar endDate = Calendar.getInstance();
	long dateRange = 0;
		
	public Album() {	
	}
	public Album(String a) {	
		this.albumName = a;
	}
	public void setName(String albumName) {
		this.albumName = albumName;
	}
	public String getName() {
		return albumName;
	}
	public void addPhoto(Photo photo) {
		photos.add(photo);
	}
	public int getNumOfPhotos() {
		numOfPhotos = photos.size();
		return numOfPhotos;
	}
	public void setStartDate() {
		try {
			startDate = photos.get(0).cal;
			for(Photo photo: photos) {
				if(photo.cal.before(startDate)){
					startDate = photo.cal;
				}
			}
		} catch(Exception e) {}
	}
	public void setEndDate() {
		try {
			endDate = photos.get(0).cal;
			for(Photo photo: photos) {
				if(photo.cal.after(endDate)){
					endDate = photo.cal;
				}
			} 
		} catch(Exception e) {}
	}
	public void setDateRange() {
		setEndDate();
		setStartDate();
        dateRange = (endDate.getTime().getTime()-startDate.getTime().getTime())/(86400000);
	}
	@Override
	public boolean equals(Object o) {	
		return this.albumName == ((Album) o).getName();
	}
}
