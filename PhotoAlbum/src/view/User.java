package view;

import java.util.ArrayList;

public class User implements java.io.Serializable {

	private static final long serialVersionUID = 4352000080082537977L;
	String userName = "";
	ArrayList<Album> albums = new ArrayList<Album>();
	int numOfAlbums = 0;
	int numOfPhotos = 0;
	
	public User() {
	}
	public User(String userName) {
		this.userName = userName;
		getNumOfAlbums();
		getAllPhotos();
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public void addAlbum(Album album) {
		albums.add(album);
	}
	public void addAlbum(String album) {
		albums.add(new Album(album));
	}
	public int getAllPhotos() {
		numOfPhotos = 0;
		try{
			for(Album album: albums) {
				numOfPhotos += album.numOfPhotos;
			}
		} catch(Exception e) {}
		return numOfPhotos;
	}
	public String getUserName() {
		return userName;
	}
	public int getNumOfAlbums() {
		numOfAlbums = albums.size();
		return numOfAlbums;
	}
}
