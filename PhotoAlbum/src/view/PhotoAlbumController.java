package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

public class PhotoAlbumController {
	// PhotoAlbum

    /**
     * FXML Handler for the back button
     */
    @FXML
    Button backButton;
    /**
     * FXML Handler for the forward button
     */
    @FXML
    Button forwardButton;
    /**
     * FXML Handler for the search button
     */
    @FXML
    Button searchButton;
    /**
     * FXML Handler for the menu button pane
     */
    @FXML
    Pane menu;
    /**
     * FXML Handler for the tile pane holding the titles
     */
    @FXML
    TilePane tile;
    /**
     * FXML Handler for the imageview that holds images
     */
    @FXML
    ImageView imageView;
    /**
     * FXML Handler for the vbox that holds the images
     */
    @FXML
    VBox vbox;
    /**
     * FXML Handler for the caption label in details
     */
    @FXML
    Label caption;
    /**
     * FXML Handler for the title label
     */
    @FXML
    Label titleLabel;
    /**
     * FXML Handler for the first banner label (top)
     */
    @FXML
    Label banner1Label;
    /**
     * FXML Handler for the second banner label (bottom)
     */
    @FXML
    Label banner2Label;
    /**
     * FXML Handler for the details box that holds all details
     */
    @FXML
    VBox details;
    /**
     * FXML Handler for the starting date search field
     */
    @FXML
    TextField searchDateStartField;
    /**
     * FXML Handler for the end date search field
     */
    @FXML
    TextField searchDateEndField;
    /**
     * FXML Handler for the tag key field in search
     */
    @FXML
    TextField searchTagField;
    /**
     * FXML Handler for the search value field
     */
    @FXML
    TextField searchValueField;

    // displayPhoto
    /**
     * FXML Handler for the next button
     */
    @FXML
    Button nextButton;
    /**
     * FXML Handler for the previous button
     */
    @FXML
    Button previousButton;
    // album
    /**
     * FXML Handler for the add photo button
     */
    @FXML
    Button addPhotoButton;
    /**
     * FXML Handler for the caption button
     */
    @FXML
    Button captionButton;
    /**
     * FXML Handler for the delete photo button
     */
    @FXML
    Button deletePhotoButton;
    /**
     * FXML Handler for the open photo button
     */
    @FXML
    Button openPhotoButton;
    /**
     * FXML Handler for the tags button
     */
    @FXML
    Button tagsButton;
    /**
     * FXML Handler for the move button
     */
    @FXML
    Button moveButton;
    // user
    /**
     * FXML Handler for the create album button
     */
    @FXML
    Button createAlbumButton;
    /**
     * FXML Handler for the rename album button
     */
    @FXML
    Button renameAlbumButton;
    /**
     * FXML Handler for the delete album button
     */
    @FXML
    Button deleteAlbumButton;
    /**
     * FXML Handler for the open album button
     */
    @FXML
    Button openAlbumButton;
    // Search
    /**
     * FXML Handler for the create album from search results button
     */
    @FXML
    Button createSeacrhButton;
    // details
    /**
     * FXML Handler for the top label of details
     */
    @FXML
    Label topLabel;
    /**
     * FXML Handler for the top label value for details
     */
    @FXML
    Label topLabelValue;
    /**
     * FXML Handler for the middle label for details
     */
    @FXML
    Label middleLabel;
    /**
     * FXML Handler for the middle label value for details
     */
    @FXML
    Label middleLabelValue;
    /**
     * FXML Handler for the bottom label for details
     */
    @FXML
    Label bottomLabel;
    /**
     * FXML Handler for the bottom label values for details
     */
    @FXML
    Label bottomLabelValue;

    /**
     * Stage that holds the last stage the interface came from
     */
    Stage prevStage;
    /**
     * Current user of the photo album
     */
    static User user;
    /**
     * Previous buttons vbox
     */
    VBox prevVBox;
    /**
     * ArrayList of the resulting photos from search
     */
    static ArrayList<Photo> resultPhotoList = new ArrayList<Photo>();
    /**
     * Count of all returned photos form a search
     */
    static int searchResultCount;
    /**
     * Name of the currently selected album
     */
    static String selectedAlbum = null;
    /**
     * Path of the currently selected photo
     */
    static String selectedPhoto = null;
    /**
     * Album that holds the currently selected album
     */
    static Album currentAlbum = null;
    /**
     * index of the currently selected photo
     */
    static int selectedPhotoIndex = 0;
    /**
     * Current state of the photoalbum program
     */
    static char state = 'u';
    /**
     * tilepane that holds all the photos
     */
    static TilePane tp = null;
    /**
     * sidemenu for the buttons
     */
    static Pane sideMenu = null;
    /**
     * tLabel for the details top label
     */
    static Label tLabel = null;
    /**
     * value of the top label in details
     */
    static Label tLabelValue = null;
    /**
     * label for the details mid label
     */
    static Label midLabel = null;
    /**
     * value of the mid label in details
     */
    static Label midLabelValue = null;
    /**
     * label for the bot label in details
     */
    static Label botLabel = null;
    /**
     * value of the bot label in details
     */
    static Label botLabelValue = null;
    /**
     * title label for the title of the photo album page
     */
    static Label stitleLabel = null;
    /**
     * label for the first banner
     */
    static Label sbanner1Label = null;
    /**
     * label for the second banner
     */
    static Label sbanner2Label = null;
//=========================================================================================================================================
//end of variables start method

    /**
     * Start method for the photo album, initializes all the labels, details, photos,
     * and albums to be displayed in the main stage.
     * 
     * @param mainStage Stage where all the information is displayed
     * @throws IOException for I/O operation exceptions
     */
    public void start(Stage mainStage) throws IOException {
        menu.getChildren().clear();
        menu.getChildren().add(FXMLLoader.load(getClass().getResource("User.fxml")));
        sideMenu = menu;
        tp = tile;
        tLabel = topLabel;
        tLabelValue = topLabelValue;
        midLabel = middleLabel;
        midLabelValue = middleLabelValue;
        botLabel = bottomLabel;
        botLabelValue = bottomLabelValue;
        stitleLabel = titleLabel;
        sbanner1Label = banner1Label;
        sbanner2Label = banner2Label;
        setAlbumsTilePane();
        setBanner();
        setDetails();
    }
//=========================================================================================================================================
//Stage change methods

    /**
     * Sets the current user of the interface to a User object
     * 
     * @param user User object to set the current user to
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Sets the previous stage for returning to where the user navigated from in the
     * interface.
     * 
     * @param stage The last stage the user navigated from
     */
    public void setPrevStage(Stage stage) {
        this.prevStage = stage;
    }

    /**
     * Sets all of the values in the banner based on the current state, be it
     * search state, album state, user state, or photo state.
     */
    public void setBanner() {
        if (state == 'u') { //user
            stitleLabel.setText("User: " + user.userName + " All Albums");
            sbanner1Label.setText("Number of Photos: " + user.getAllPhotos());
            sbanner2Label.setText("Number of Albums: " + user.getNumOfAlbums());
        } else if (state == 'a') { //album 
            stitleLabel.setText("User: " + user.userName + " Album view");
            sbanner1Label.setText("Album Title: " + currentAlbum.albumName);
            sbanner2Label.setText("Number of Photos: " + currentAlbum.getNumOfPhotos());
        } else if (state == 'p') { //display photo
            stitleLabel.setText("User: " + user.userName + " SlideShow");
            sbanner1Label.setText("Album Title: " + currentAlbum.albumName);
            sbanner2Label.setText("Number of Photos: " + currentAlbum.getNumOfPhotos());
        } else { //search
            stitleLabel.setText("User: " + user.userName + " Search Results");
            sbanner1Label.setText("Number of Photos: " + searchResultCount);
            sbanner2Label.setText("");
        }
    }

    /**
     * Sets the details tab on the right based on the current state of the UI
     * and the currently selected photo.
     * 
     * @throws IOException for I/O operation errors
     */
    public void setDetails() throws IOException {
        if (state == 'u') { //user
            tLabel.setText("Album Title");
            midLabel.setText("Photo Count");
            botLabel.setText("Date Range(Days)");

            if (selectedAlbum != null) {
                for (Album a : user.albums) {
                    if (a.albumName.equalsIgnoreCase(selectedAlbum)) {
                        tLabelValue.setText(a.getName());
                        midLabelValue.setText("" + a.getNumOfPhotos());
                        a.setDateRange();
                        botLabelValue.setText("               " + a.dateRange + "\n   "
                                + "          Oldest \n" + a.endDate.getTime());
                        break;
                    }
                }
            }
        } else if (state == 'a' || state == 'p') { //album, display photo 
            tLabel.setText("Caption");
            midLabel.setText("Date last modified");
            botLabel.setText("Tags");
            if (selectedPhoto != null) {
                for (Photo p : currentAlbum.photos) {
                    if (p.path.equalsIgnoreCase(selectedPhoto)) {
                        tLabelValue.setText(p.caption);
                        midLabelValue.setText("" + p.cal.getTime());
                        botLabelValue.setText(p.toStringtags(p));
                        break;
                    }
                }
            }
        } else if (state == 's') { //search
            tLabel.setText("Caption");
            midLabel.setText("Date last modified");
            botLabel.setText("Tags");
            boolean br = false;
            for (Album a : user.albums) {
                for (Photo p : a.photos) {
                    if (p.path.equals(selectedPhoto)) {
                        br = true;
                        tLabelValue.setText(p.caption);
                        midLabelValue.setText("" + p.cal.getTime());
                        botLabelValue.setText(p.toStringtags(p));
                        break;
                    }
                }
                if (br) {
                    break;
                }
            }
        }
    }
//=========================================================================================================================================
// Setting the tile pane methods

    /**
     * Displays the tile of albums in the album view, showing all the current albums
     * that the current user has with a thumbnail and their album names.
     * 
     * @throws FileNotFoundException for incorrect file calls
     */
    public void setAlbumsTilePane() throws FileNotFoundException {
        tp.getChildren().clear();
        boolean hasAlbums = false;
        try {
            for (Album album : user.albums) {
                hasAlbums = true;
                File file = null;
                boolean hasPhotos = false;
                // get thumb nail image for album
                try {
                    file = new File(album.photos.get(0).path);
                    hasPhotos = true;
                } catch (Exception e) {
                }
                if (!hasPhotos) {
                    file = new File("photos/noImage.png");
                }
                // make image objects 
                Image image = new Image(new FileInputStream(file), 100, 60, true, true);

                // Make ImageView for vbox
                ImageView imageView = new ImageView(image);
                imageView.maxHeight(60);
                imageView.maxWidth(100);

                // Make label for album name for vbox
                Label name = new Label();
                name.setPrefWidth(100);
                name.setPrefHeight(15);
                name.setText(album.albumName);
                Label numPhotos = null;
                Label oldestPhoto = null;
                Label range = null;

                if (hasPhotos) {
                    // Make label for # of photos for vbox
                    numPhotos = new Label();
                    numPhotos.setPrefWidth(100);
                    numPhotos.setPrefHeight(15);
                    numPhotos.setText("" + album.getNumOfPhotos());
                    album.setDateRange();
                    // Make label for oldest photo for vbox
                    oldestPhoto = new Label();
                    oldestPhoto.setPrefWidth(100);
                    oldestPhoto.setPrefHeight(15);
                    oldestPhoto.setText("oldest: " + album.startDate.getTime().toString());
                    // Make label for range of dates for vbox
                    range = new Label();
                    range.setPrefWidth(100);
                    range.setPrefHeight(15);
                    range.setText("days: " + album.dateRange);
                }
		        // make vbox to add as tile to tilepane
                // Add details and image to vbox 
                VBox vbox = new VBox();
                vbox.setId(album.albumName);
                if (!hasPhotos) {
                    vbox.setPrefWidth(100);
                    vbox.setPrefHeight(75);
                } else {
                    vbox.setPrefWidth(100);
                    vbox.setPrefHeight(120);
                }
                vbox.getChildren().add(imageView);
                vbox.getChildren().add(name);
                if (hasPhotos) {
                    vbox.getChildren().add(numPhotos);
                    vbox.getChildren().add(oldestPhoto);
                    vbox.getChildren().add(range);
                }
                // Add mouse click event listener to vbox
                vbox.setOnMouseClicked(e -> {
                    if (prevVBox != null) {
                        prevVBox.setStyle("-fx-background-color: #F4F4F4;");
                    }
                    prevVBox = vbox;
                    vbox.setStyle("-fx-background-color: #6495ed80;");
                    selectedAlbum = ((Node) e.getSource()).getId();
                    try {
                        setDetails();
                        setBanner();
                    } catch (Exception e1) {
                    }
                });
                // add vbox to tile pane
                tp.getChildren().add(vbox);
            }
        } catch (Exception e) {
        }

        if (!hasAlbums) {
            File file = null;
            // get thumb nail image for album
            file = new File("photos/emptyFolder.png");
            // make image objects 
            Image image = new Image(new FileInputStream(file), 100, 60, true, true);

            // Make ImageView for vbox
            ImageView imageView = new ImageView(image);
            imageView.maxHeight(60);
            imageView.maxWidth(100);

            // Make label for empty album  for vbox
            Label name = new Label();
            name.setPrefWidth(100);
            name.setPrefHeight(15);
            name.setText("no albums");

            // make vbox to add as tile to tilepane
            VBox vbox = new VBox();
            vbox.setId(null);
            vbox.setPrefWidth(100);
            vbox.setPrefHeight(120);
            vbox.getChildren().add(imageView);
            vbox.getChildren().add(name);
            // Add mouse click event listener to vbox
            vbox.setOnMouseClicked(ex -> {
                if (prevVBox != null) {
                    prevVBox.setStyle("-fx-background-color: #F4F4F4;");
                }
                prevVBox = vbox;
                vbox.setStyle("-fx-background-color: #6495ed80;");
                selectedAlbum = ((Node) ex.getSource()).getId();
                try {
                    setDetails();
                    setBanner();
                } catch (Exception e) {
                }
            });
            // add vbox to tile pane
            tp.getChildren().add(vbox);
        }
    }

    /**
     * 
     * @throws FileNotFoundException 
     */
    public void setPhotoTilePane() throws FileNotFoundException {
        tp.getChildren().clear();

        boolean hasPhotos = false;
        try {
            for (Photo photo : currentAlbum.photos) {
                hasPhotos = true;
                File file = null;
                // get thumb nail image for album
                file = new File(photo.path);

                // make image objects 
                Image image = new Image(new FileInputStream(file), 100, 60, true, true);

                // Make ImageView for vbox
                ImageView imageView = new ImageView(image);
                imageView.maxHeight(60);
                imageView.maxWidth(100);

                // Make label for album name for vbox
                Label name = new Label();
                name.setPrefWidth(100);
                name.setPrefHeight(15);
                name.setText(photo.caption);

		        // make vbox to add as tile to tilepane
                // Add details and image to vbox 
                VBox vbox = new VBox();
                vbox.setId(photo.path);
                vbox.setPrefWidth(100);
                vbox.setPrefHeight(75);
                vbox.getChildren().add(imageView);
                vbox.getChildren().add(name);
                // Add mouse click event listener to vbox
                vbox.setOnMouseClicked(e -> {
                    if (prevVBox != null) {
                        prevVBox.setStyle("-fx-background-color: #F4F4F4;");
                    }
                    prevVBox = vbox;
                    vbox.setStyle("-fx-background-color: #6495ed80;");
                    selectedPhoto = ((Node) e.getSource()).getId();
                    try {
                        setDetails();
                        setBanner();
                    } catch (Exception e1) {
                    }
                });
                // add vbox to tile pane
                tp.getChildren().add(vbox);
            }
        } catch (Exception e) {
        }

        if (!hasPhotos) {
            File file = null;
            // get thumb nail image for album
            file = new File("photos/noImage.png");
            // make image objects 
            Image image = new Image(new FileInputStream(file), 100, 60, true, true);

            // Make ImageView for vbox
            ImageView imageView = new ImageView(image);
            imageView.maxHeight(60);
            imageView.maxWidth(100);

            // Make label for empty album  for vbox
            Label name = new Label();
            name.setPrefWidth(100);
            name.setPrefHeight(15);
            name.setText("no image");

            // make vbox to add as tile to tilepane
            VBox vbox = new VBox();
            vbox.setId(null);
            vbox.setPrefWidth(100);
            vbox.setPrefHeight(75);
            vbox.getChildren().add(imageView);
            vbox.getChildren().add(name);
            // Add mouse click event listener to vbox
            vbox.setOnMouseClicked(ex -> {
                if (prevVBox != null) {
                    prevVBox.setStyle("-fx-background-color: #F4F4F4;");
                }
                prevVBox = vbox;
                vbox.setStyle("-fx-background-color: #6495ed80;");
                selectedPhoto = ((Node) ex.getSource()).getId();
                try {
                    setDetails();
                    setBanner();
                } catch (Exception e) {
                }
            });
            // add vbox to tile pane
            tp.getChildren().add(vbox);
        }
    }

    public void setDisplayPhotoTilePane() throws FileNotFoundException {
        tp.getChildren().clear();

        try {
            File file = null;
            // get photo file
            file = new File(selectedPhoto);

            // get photo object
            Photo displayedPhoto = null;
            int i = 0;
            for (Photo p : currentAlbum.photos) {
                if (selectedPhoto.equals(p.path)) {
                    displayedPhoto = p;
                    break;
                }
                i++;
            }
            selectedPhotoIndex = i;
            // make image objects 
            Image image = new Image(new FileInputStream(file), 342, 346, true, true);

            // Make ImageView for AnchorPane
            ImageView imageView = new ImageView(image);
            imageView.maxWidth(347);
            imageView.maxHeight(341);

		    // make AnchorPane to add as tile to tilepane
            // Add details and image to AnchorPane 
            AnchorPane anchorPane = new AnchorPane();
            anchorPane.setId(displayedPhoto.path);
            anchorPane.getChildren().add(imageView);
            setDetails();
            setBanner();
            // add AnchorPane to tile pane
            tp.getChildren().add(anchorPane);
        } catch (Exception e) {
        }
    }
//=========================================================================================================================================
//button handlers for album display

    public void handleCreateAlbumButton(ActionEvent e) throws IOException {
        //add Dialog
        Dialog<ButtonType> addAlbumDialog = new Dialog<ButtonType>();
        addAlbumDialog.setTitle("Create new Album");

        //Create Buttons
        ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
        addAlbumDialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        //Create Field and label
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        TextField albumName = new TextField();
        albumName.setPromptText("Album Name");
        grid.add(new Label("New Album Name:"), 0, 0);
        grid.add(albumName, 1, 0);

        //Enalbe/disable Button
        Node okButton = addAlbumDialog.getDialogPane().lookupButton(loginButtonType);
        okButton.setDisable(true);

        albumName.textProperty().addListener((observable, oldValue, newValue) -> {
            okButton.setDisable(true);
            boolean validAlbumName = true;
            try {
                for (Album a : user.albums) {
                    if (a.albumName.equalsIgnoreCase(newValue)) {
                        validAlbumName = false;
                        break;
                    }
                }
            } catch (Exception e2) {
            }
            if (validAlbumName) {
                okButton.setDisable(false);
            }
        });

        addAlbumDialog.getDialogPane().setContent(grid);

        Platform.runLater(() -> albumName.requestFocus());

        Optional<ButtonType> result = addAlbumDialog.showAndWait();
        if (result.isPresent() && !albumName.getText().equals("") && result.get() == loginButtonType) {
            user.addAlbum(albumName.getText());
            setAlbumsTilePane();
            setBanner();
            setDetails();
        }
    }

    public void handleRenameAlbumButton(ActionEvent e) throws IOException {
        if (selectedAlbum != null) {
            //Rename Dialog
            Dialog<ButtonType> renameAlbumDialog = new Dialog<ButtonType>();
            renameAlbumDialog.setTitle("Rename Album");
            renameAlbumDialog.setHeaderText("Old Name: '" + selectedAlbum + "'");

            //Create Buttons
            ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
            renameAlbumDialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

            //Create Field and label
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));
            TextField albumName = new TextField();
            albumName.setPromptText("Album Name");
            grid.add(new Label("New Album Name:"), 0, 0);
            grid.add(albumName, 1, 0);

            //Enalbe/disable Button
            Node okButton = renameAlbumDialog.getDialogPane().lookupButton(loginButtonType);
            okButton.setDisable(true);

            albumName.textProperty().addListener((observable, oldValue, newValue) -> {
                okButton.setDisable(true);
                boolean validAlbumName = true;
                for (Album a : user.albums) {
                    if (a.albumName.equalsIgnoreCase(newValue)) {
                        validAlbumName = false;
                    }
                }
                if (validAlbumName) {
                    okButton.setDisable(false);
                }
            });

            renameAlbumDialog.getDialogPane().setContent(grid);

            Platform.runLater(() -> albumName.requestFocus());
            Optional<ButtonType> result = renameAlbumDialog.showAndWait();
            if (result.isPresent() && !albumName.getText().equals("") && result.get() == loginButtonType) {
                for (Album a : user.albums) {
                    if (a.albumName.equalsIgnoreCase(selectedAlbum)) {
                        a.setName(albumName.getText());
                        break;
                    }
                }
                setAlbumsTilePane();
                setBanner();
                setDetails();
            }
        } else {
            Alert noSelectionAlert = new Alert(AlertType.ERROR);
            noSelectionAlert.setTitle("No album selected");
            noSelectionAlert.setHeaderText("Please select an album by clicking on it");
            noSelectionAlert.showAndWait();
        }
    }

    public void handleDeleteAlbumButton(ActionEvent e) throws IOException {
        if (selectedAlbum != null) {
            Alert deleteAlert = new Alert(AlertType.CONFIRMATION);
            deleteAlert.setTitle("Confirmation Dialog");
            deleteAlert.setHeaderText("Delete Album '" + selectedAlbum + "'");
            deleteAlert.setContentText("Are you sure?");

            Optional<ButtonType> result = deleteAlert.showAndWait();
            if (result.get() == ButtonType.OK) {
                for (Album a : user.albums) {
                    if (a.albumName.equalsIgnoreCase(selectedAlbum)) {
                        user.albums.remove(a);
                        selectedAlbum = null;
                        break;
                    }
                }
                setAlbumsTilePane();
                setBanner();
                setDetails();
            }
        } else {
            Alert noSelectionAlert = new Alert(AlertType.ERROR);
            noSelectionAlert.setTitle("No album selected");
            noSelectionAlert.setHeaderText("Please select an album by clicking on it");
            noSelectionAlert.showAndWait();
        }
    }

    public void handleOpenAlbumButton(ActionEvent e) throws IOException {
        if (selectedAlbum != null) {
            state = 'a'; // album state
            sideMenu.getChildren().clear();
            sideMenu.getChildren().add(FXMLLoader.load(getClass().getResource("Album.fxml")));
            for (Album a : user.albums) {
                if (a.albumName.equalsIgnoreCase(selectedAlbum)) {
                    currentAlbum = a;
                    break;
                }
            }
            setPhotoTilePane();
            setBanner();
            setDetails();
        } else {
            Alert noSelectionAlert = new Alert(AlertType.ERROR);
            noSelectionAlert.setTitle("No Album Selected");
            noSelectionAlert.setHeaderText("Please select an album by clicking on it");
            noSelectionAlert.showAndWait();
        }
    }
//=========================================================================================================================================
//button handlers for album display

    public void handleOpenPhotoButton(ActionEvent e) throws IOException {
        if (selectedPhoto != null) {
            state = 'p'; // Display photo
            sideMenu.getChildren().clear();
            sideMenu.getChildren().add(FXMLLoader.load(getClass().getResource("DisplayPhoto.fxml")));

            setDisplayPhotoTilePane();
            setBanner();
            setDetails();
        } else {
            Alert noSelectionAlert = new Alert(AlertType.ERROR);
            noSelectionAlert.setTitle("No Photo Selected");
            noSelectionAlert.setHeaderText("Please select an photo by clicking on it");
            noSelectionAlert.showAndWait();
        }
    }

    public void handleAddPhotoButton(ActionEvent e) throws IOException {
        //add Dialog
        Dialog<ButtonType> addPhotoDialog = new Dialog<ButtonType>();
        addPhotoDialog.setTitle("Create new Pohto");

        //Create Buttons
        ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
        addPhotoDialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        //Create Field and label
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        TextField photoPath = new TextField();
        photoPath.setPromptText("Photo's Path");
        grid.add(new Label("File path to photo:"), 0, 0);
        grid.add(photoPath, 1, 0);

        //Enalbe/disable Button
        Node okButton = addPhotoDialog.getDialogPane().lookupButton(loginButtonType);
        okButton.setDisable(true);

        photoPath.textProperty().addListener((observable, oldValue, newValue) -> {
            okButton.setDisable(true);
            boolean duplicatePhotoCheck = true;
            try {
                for (Photo p : currentAlbum.photos) {
                    if (p.path.equalsIgnoreCase(newValue)) {
                        duplicatePhotoCheck = false;
                    }
                }
            } catch (Exception e2) {
            }
            if (duplicatePhotoCheck) {
                okButton.setDisable(false);
            }
        });

        addPhotoDialog.getDialogPane().setContent(grid);

        Platform.runLater(() -> photoPath.requestFocus());

        Optional<ButtonType> result = addPhotoDialog.showAndWait();
        if (result.isPresent() && !photoPath.getText().equals("") && result.get() == loginButtonType) {
            File photoFile = new File(photoPath.getText());
            if (!photoFile.exists() && !photoFile.isDirectory()) {
                Alert filePathAlert = new Alert(AlertType.ERROR);
                filePathAlert.setTitle("Image not found");
                filePathAlert.setHeaderText("Could not find image at location");
                filePathAlert.showAndWait();
            } else {
                currentAlbum.addPhoto(new Photo(photoPath.getText()));
                setPhotoTilePane();
                setBanner();
                setDetails();
            }
        }
    }

    public void handleCaptionButton(ActionEvent e) throws IOException {
        //Rename Dialog
        if (selectedPhoto != null) {
            Photo reCaptionPhoto = null;
            for (Photo p : currentAlbum.photos) {
                if (p.path.equals(selectedPhoto)) {
                    reCaptionPhoto = p;
                }
            }
            Dialog<ButtonType> captionDialog = new Dialog<ButtonType>();
            captionDialog.setTitle("Add/ReCaption Photo");
            captionDialog.setHeaderText("Old Caption: '" + reCaptionPhoto.caption + "'");

            //Create Buttons
            ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
            captionDialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

            //Create Field and label
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));
            TextField newCaption = new TextField();
            newCaption.setPromptText("Caption");
            grid.add(new Label("New Caption:"), 0, 0);
            grid.add(newCaption, 1, 0);

            //Enalbe/disable Button
            Node okButton = captionDialog.getDialogPane().lookupButton(loginButtonType);

            captionDialog.getDialogPane().setContent(grid);

            Platform.runLater(() -> newCaption.requestFocus());
            Optional<ButtonType> result = captionDialog.showAndWait();
            if (result.isPresent() && !newCaption.getText().equals("") && result.get() == loginButtonType) {
                reCaptionPhoto.setCaption(newCaption.getText());
                selectedPhoto = null;
                setPhotoTilePane();
                setBanner();
                setDetails();
            }
        } else {
            Alert noSelectionAlert = new Alert(AlertType.ERROR);
            noSelectionAlert.setTitle("No Photo Selected");
            noSelectionAlert.setHeaderText("Please select an photo by clicking on it");
            noSelectionAlert.showAndWait();
        }
    }

    public void handleadDeletePhotoButton(ActionEvent e) throws IOException {
        if (selectedPhoto != null) {
            Photo photo = new Photo(selectedPhoto);
            Alert deleteAlert = new Alert(AlertType.CONFIRMATION);
            deleteAlert.setTitle("Confirmation Dialog");
            deleteAlert.setHeaderText("Delete Photo '" + photo.caption + "'");
            File file = new File(photo.path);
            Image image = new Image(new FileInputStream(file), 100, 60, true, true);
            ImageView imageView = new ImageView(image);
            imageView.maxHeight(60);
            imageView.maxWidth(100);
            deleteAlert.setGraphic(imageView);
            deleteAlert.setContentText("Are you sure?");

            Optional<ButtonType> result = deleteAlert.showAndWait();
            if (result.get() == ButtonType.OK) {

                for (Photo p : currentAlbum.photos) {
                    if (p.path.equalsIgnoreCase(selectedPhoto)) {
                        currentAlbum.photos.remove(p);
                        selectedPhoto = null;
                        break;
                    }
                }
                setPhotoTilePane();
                setBanner();
                setDetails();
            }
        } else {
            Alert noSelectionAlert = new Alert(AlertType.ERROR);
            noSelectionAlert.setTitle("No photo selected");
            noSelectionAlert.setHeaderText("Please select an photo by clicking on it");
            noSelectionAlert.showAndWait();
        }
    }

    public void handleMoveButton(ActionEvent e) throws IOException {
        //add Dialog
        if (selectedPhoto != null) {
            Photo movingPhoto = new Photo(selectedPhoto);
            Dialog<ButtonType> movePhotoDialog = new Dialog<ButtonType>();
            movePhotoDialog.setTitle("Move '" + movingPhoto.caption + "' to new Album");

            //Create Buttons
            ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
            movePhotoDialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
            File file = new File(movingPhoto.path);
            Image image = new Image(new FileInputStream(file), 100, 60, true, true);
            ImageView imageView = new ImageView(image);
            imageView.maxHeight(60);
            imageView.maxWidth(100);
            movePhotoDialog.setGraphic(imageView);
            //Create Field and label
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));
            TextField albumName = new TextField();
            albumName.setPromptText("Album Name");
            grid.add(new Label("New Album Name:"), 0, 0);
            grid.add(albumName, 1, 0);

            //Enalbe/disable Button
            Node okButton = movePhotoDialog.getDialogPane().lookupButton(loginButtonType);
            okButton.setDisable(true);

            albumName.textProperty().addListener((observable, oldValue, newValue) -> {
                okButton.setDisable(true);
                boolean validAlbumName = false;
                try {
                    for (Album a : user.albums) {
                        if (a.albumName.equalsIgnoreCase(newValue)) {
                            validAlbumName = true;
                            break;
                        }
                    }
                } catch (Exception e2) {
                }
                if (validAlbumName) {
                    okButton.setDisable(false);
                }
            });

            movePhotoDialog.getDialogPane().setContent(grid);

            Platform.runLater(() -> albumName.requestFocus());

            Optional<ButtonType> result = movePhotoDialog.showAndWait();
            if (result.isPresent() && !albumName.getText().equals("") && result.get() == loginButtonType) {
                Album newAlbum = null;

                for (Album a : user.albums) {
                    if (a.albumName.equalsIgnoreCase(albumName.getText())) {
                        newAlbum = a;
                        break;
                    }
                }
                boolean photoAlreadyExist = false;
                try {
                    for (Photo p : newAlbum.photos) {
                        if (movingPhoto.equals(p)) {
                            photoAlreadyExist = true;
                            break;
                        }
                    }
                } catch (Exception e1) {
                }
                if (photoAlreadyExist) {
                    Alert noSelectionAlert = new Alert(AlertType.ERROR);
                    noSelectionAlert.setTitle("Photo already exist");
                    noSelectionAlert.setHeaderText("Photo already exist in Album " + albumName.getText() + ", can not move");
                    noSelectionAlert.showAndWait();
                } else {
                    newAlbum.addPhoto(movingPhoto);
                    currentAlbum.photos.remove(movingPhoto);
                    selectedPhoto = null;
                    setPhotoTilePane();
                    setBanner();
                    setDetails();
                }
            }
        } else {
            Alert noSelectionAlert = new Alert(AlertType.ERROR);
            noSelectionAlert.setTitle("No photo selected");
            noSelectionAlert.setHeaderText("Please select an photo by clicking on it");
            noSelectionAlert.showAndWait();
        }
    }

    public void handleTagsButton(ActionEvent e) throws IOException {
        if (selectedPhoto != null) {
            try {
                Photo thisPhoto = null;
                for (Photo p : currentAlbum.photos) {
                    if (p.equals(new Photo(selectedPhoto))) {
                        thisPhoto = p;
                        break;
                    }
                }
                
                FXMLLoader loader = new FXMLLoader(getClass().getResource("TagEditor.fxml"));
                Parent root1 = (Parent) loader.load();
                Stage stage = new Stage();
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.initStyle(StageStyle.DECORATED);
                stage.setTitle("Tags Editor");
                stage.setScene(new Scene(root1));
                TagEditorController controller = (TagEditorController) loader.getController();
                controller.getInfo(thisPhoto, user, currentAlbum);
                controller.start(stage);
                stage.show();
                stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
					public void handle(WindowEvent we) {
						try {
							setDetails();
						} catch (IOException e) {
						}
			            setBanner();
			        }
			    });
            } catch (Exception e1) {
            }
        } else {
            Alert noSelectionAlert = new Alert(AlertType.ERROR);
            noSelectionAlert.setTitle("No photo selected");
            noSelectionAlert.setHeaderText("Please select an photo by clicking on it");
            noSelectionAlert.showAndWait();
        }
    }
//=========================================================================================================================================
//buttons in display photo state

    public void handleNextButton(ActionEvent e) throws IOException {
        try {
            selectedPhoto = currentAlbum.photos.get(selectedPhotoIndex + 1).path;
            selectedPhotoIndex++;
            setDisplayPhotoTilePane();
        } catch (Exception e1) {
        }
    }

    public void handlePreviousButton(ActionEvent e) throws IOException {
        try {
            selectedPhoto = currentAlbum.photos.get(selectedPhotoIndex - 1).path;
            selectedPhotoIndex--;
            setDisplayPhotoTilePane();
        } catch (Exception e1) {
        }
    }

//=========================================================================================================================================
//buttons in All all Search	
    public void handleCreateSeacrhButton(ActionEvent e) throws IOException {
        if (!resultPhotoList.isEmpty()) {
            Dialog<ButtonType> addAlbumDialog = new Dialog<ButtonType>();
            addAlbumDialog.setTitle("Create new Album From Search");

            //Create Buttons
            ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
            addAlbumDialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

            //Create Field and label
            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));
            TextField albumName = new TextField();
            albumName.setPromptText("Album Name");
            grid.add(new Label("New Album Name:"), 0, 0);
            grid.add(albumName, 1, 0);

            //Enalbe/disable Button
            Node okButton = addAlbumDialog.getDialogPane().lookupButton(loginButtonType);
            okButton.setDisable(true);

            albumName.textProperty().addListener((observable, oldValue, newValue) -> {
                okButton.setDisable(true);
                boolean validAlbumName = true;
                try {
                    for (Album a : user.albums) {
                        if (a.albumName.equalsIgnoreCase(newValue)) {
                            validAlbumName = false;
                            break;
                        }
                    }
                } catch (Exception e2) {
                }
                if (validAlbumName) {
                    okButton.setDisable(false);
                }
            });

            addAlbumDialog.getDialogPane().setContent(grid);

            Platform.runLater(() -> albumName.requestFocus());

            Optional<ButtonType> result = addAlbumDialog.showAndWait();
            if (result.isPresent() && !albumName.getText().equals("") && result.get() == loginButtonType) {
                Album newAlbum = new Album();
                newAlbum.setName(albumName.getText());
                for (Photo p : resultPhotoList) {
                    newAlbum.addPhoto(p);
                }
                user.addAlbum(newAlbum);
                setAlbumsTilePane();
                setBanner();
                setDetails();
            }
        }
    }
//=========================================================================================================================================
//buttons in All all states

    public void handleSearchButton(ActionEvent e) throws ParseException, IOException {
        //Get all Photos for current user
        resultPhotoList.clear();
        ArrayList<Photo> searchPhotoList = new ArrayList<Photo>();
        boolean dupPhoto = false;
        try {
            for (Album a : user.albums) {
                try {
                    for (Photo b : a.photos) {
                        dupPhoto = false;
                        for (Photo c : searchPhotoList) {
                            if (c.path.equals(b.path)) {
                                dupPhoto = true;
                            }
                        }
                        if (!dupPhoto) {
                            searchPhotoList.add(b);
                        }
                    }
                } catch (Exception e2) {
                };
            }
        } catch (Exception e1) {
        };

        String searchDateStart = searchDateStartField.getText();
        String searchDateEnd = searchDateEndField.getText();
        String searchTag = searchTagField.getText();
        String searchValue = searchValueField.getText();

        boolean didSearch = false;
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dateFormat.setLenient(false);
        if (!searchDateStart.isEmpty() && !searchDateEnd.isEmpty()) {
            boolean isValidStart, isValidEnd;
            try {
                dateFormat.parse(searchDateStart);
                isValidStart = true;
            } catch (ParseException ex) {
                isValidStart = false;
            }
            try {
                dateFormat.parse(searchDateEnd);
                isValidEnd = true;
            } catch (ParseException ex) {
                isValidEnd = false;
            }
            //Both are Valid dates
            if (isValidStart && isValidEnd) {
                //Check if dates are chornological
                Date dateStart = dateFormat.parse(searchDateStart);
                Date dateEnd = dateFormat.parse(searchDateEnd);
                if (dateStart.before(dateEnd) || dateStart.equals(dateEnd)) {
                    //Valid Date, check if searching both date and tags validly
                    didSearch = true;
                    if (!searchTag.isEmpty() && !searchValue.isEmpty()) {
                        for (Photo a : searchPhotoList) {
                            if ((a.cal.getTime().after(dateStart) && a.cal.getTime().before(dateEnd))
                                    || (a.cal.getTime().equals(dateStart) || a.cal.getTime().equals(dateEnd))) {
                                for (Pair<String, String> b : a.tags) {
                                    if (b.getKey().equalsIgnoreCase(searchTag) && b.getValue().equalsIgnoreCase(searchValue)) {
                                        resultPhotoList.add(a);
                                    }
                                }
                            }
                        }
                    } else {
                        for (Photo a : searchPhotoList) {
                            if ((a.cal.getTime().after(dateStart) && a.cal.getTime().before(dateEnd))
                                    || (a.cal.getTime().equals(dateStart) || a.cal.getTime().equals(dateEnd))) {
                                resultPhotoList.add(a);
                            }
                        }
                    }
                } else {
                    Alert alert3 = new Alert(AlertType.INFORMATION);
                    alert3.setTitle("Search Dialog");
                    alert3.setHeaderText("Invalid Search");
                    alert3.setContentText("End date is before start date.");

                    alert3.showAndWait();
                }
            } else {
                Alert alert2 = new Alert(AlertType.INFORMATION);
                alert2.setTitle("Search Dialog");
                alert2.setHeaderText("Invalid Search");
                alert2.setContentText("Invalid date paramaters given.");

                alert2.showAndWait();
            }
        } //They searched for tags
        else if (searchDateStart.isEmpty() && searchDateEnd.isEmpty() && !searchTag.isEmpty() && !searchValue.isEmpty()) {
            didSearch = true;
            for (Photo a : searchPhotoList) {
                for (Pair<String, String> b : a.tags) {
                    if (b.getKey().equalsIgnoreCase(searchTag) && b.getValue().equalsIgnoreCase(searchValue)) {
                        resultPhotoList.add(a);
                    }
                }
            }
        }

        //Check if there is a result
        if (!resultPhotoList.isEmpty()) {
            tp.getChildren().clear();
            sideMenu.getChildren().clear();
            sideMenu.getChildren().add(FXMLLoader.load(getClass().getResource("Search.fxml")));
            state = 's';
            for (Photo c : resultPhotoList) {
                File file = new File(c.path);
                // make image objects 
                Image image = new Image(new FileInputStream(file), 100, 60, true, true);

                // Make ImageView for vbox
                ImageView imageView = new ImageView(image);
                imageView.maxHeight(60);
                imageView.maxWidth(100);

                // Make label for empty album  for vbox
                Label name = new Label();
                name.setPrefWidth(100);
                name.setPrefHeight(15);
                name.setText(c.caption);

                // make vbox to add as tile to tilepane
                VBox vbox = new VBox();
                vbox.setId(c.path);
                vbox.setPrefWidth(100);
                vbox.setPrefHeight(120);
                vbox.getChildren().add(imageView);
                vbox.getChildren().add(name);
                setDetails();
                setBanner();
                // Add mouse click event listener to vbox
                vbox.setOnMouseClicked(ex -> {
                    if (prevVBox != null) {
                        prevVBox.setStyle("-fx-background-color: #F4F4F4;");
                    }
                    prevVBox = vbox;
                    vbox.setStyle("-fx-background-color: #6495ed80;");
                    selectedPhoto = ((Node) ex.getSource()).getId();
                    try {
                        setDetails();
                        setBanner();
                    } catch (Exception f) {
                    }
                });
                // add vbox to tile pane
                tp.getChildren().add(vbox);
                searchResultCount = resultPhotoList.size();
            }
        } else if (didSearch) {
            //NO results from search
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Search Dialog");
            alert.setHeaderText("Search Completed");
            alert.setContentText("No photos found from search.");

            alert.showAndWait();
        }
    }

    public void handleBackButton(ActionEvent e) throws IOException {
        if (state == 'p') { // display photo, return to album
            state = 'a';
            sideMenu.getChildren().clear();
            sideMenu.getChildren().add(FXMLLoader.load(getClass().getResource("Album.fxml")));
            setPhotoTilePane();
            setBanner();
            setDetails();
        } else if (state == 'a' || state == 's') { //album or Search, return to user
            state = 'u';
            sideMenu.getChildren().clear();
            sideMenu.getChildren().add(FXMLLoader.load(getClass().getResource("User.fxml")));
            setAlbumsTilePane();
            setBanner();
            setDetails();
        }
        // else in user, do nothing disable button 
    }
}