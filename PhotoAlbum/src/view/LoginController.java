package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javafx.scene.layout.Pane;
import view.AdminController;

public class LoginController implements java.io.Serializable {

	@FXML Button enterButton;
	@FXML TextField usernameField;
	@FXML Label errorLabel;
	
	String adminUsername = "admin";
	ArrayList<User> userList = new ArrayList<User>();

	private Button b;
	Stage prevStage;
	private ObjectInputStream obj_in;
	private ObjectOutputStream obj_out;

	public void setPrevStage(Stage stage){
		this.prevStage = stage;
	}
	public User getUsername(int x){
	    return userList.get(x);
	}
	public void start(Stage mainStage) throws IOException, ClassNotFoundException {   
	    File usernameFile = new File("userList.ser");
	    
	    if (usernameFile.exists() && !usernameFile.isDirectory()){
	        //Read from disk using FileInputStream
	        FileInputStream f_in = new FileInputStream("userList.ser");
	        obj_in = new ObjectInputStream(f_in);
	        //Read an object
	        Object obj = obj_in.readObject();
	        if (obj instanceof ArrayList)
	        {
	            //Cast object to ArrayList
	            userList = (ArrayList<User>) obj;
	        }
	        f_in.close();
	        obj_in.close();
	    }
	    
	    mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
	    	private File usernameFile;

			public void handle(WindowEvent we) {
	    	    usernameFile = new File("userList.ser");
				try {
					FileOutputStream f_out = new FileOutputStream("userList.ser");
					obj_out = new ObjectOutputStream(f_out);
					obj_out.writeObject(userList);
			        f_out.close();
			        obj_out.close();
				} catch (IOException e) {
				}
	        }
	    });   
	}
	
	public void handleButtonEnter(ActionEvent e) throws IOException, ClassNotFoundException {
		b = (Button)e.getSource();
	    errorLabel.setText("");
		if (b == enterButton) {
		    if (usernameField.getText().equalsIgnoreCase(adminUsername)) {
		    	Stage stage = new Stage();
			    Pane myPane = null;
				FXMLLoader loader = new FXMLLoader(getClass().getResource("Admin.fxml"));
				myPane = (Pane) loader.load();
				AdminController controller = (AdminController) loader.getController();
				
				controller.setPrevStage(stage);
				controller.start(stage);		

				Scene scene = new Scene(myPane);
			    stage.setScene(scene);
			    prevStage.close();
			    stage.setResizable(false);
			    stage.show();
				
			    stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

					public void handle(WindowEvent we) {
			        	  Stage stage = new Stage();
			  		      Pane myPane = null;
			  			  FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"));
			  			  
			  			  try {
							myPane = (Pane) loader.load();
						  } catch (IOException e) {
						  }
			  			  
			  			  LoginController controller = (LoginController) loader.getController();
			  			  controller.setPrevStage(stage);
			  			  
			  			  try {
							controller.start(stage);
			  			  } catch (ClassNotFoundException | IOException e) {
			  			  }		

			  		 	  Scene scene = new Scene(myPane);
			  		      stage.setScene(scene);
			  		      prevStage.close();
			  		      stage.setResizable(false);
			  		      stage.show(); 
			          }
			    });
		    } else {
		    	boolean checkUsername = false;
		    	User logedIn = null;
		        for (User u: userList){
		            if (usernameField.getText().equalsIgnoreCase(u.getUserName())) {
		            	logedIn = u;
		                checkUsername = true;
		                break;
		            }
		        }
		        if (!checkUsername) {
		            errorLabel.setText("Invalid Username");
			    } else {
					Stage stage = new Stage();
				    Pane myPane = null;
					FXMLLoader loader = new FXMLLoader(getClass().getResource("PhotoAlbum.fxml"));
					myPane = (Pane) loader.load();
					PhotoAlbumController controller = (PhotoAlbumController) loader.getController();
					controller.setUser(logedIn);
					controller.setPrevStage(stage);
					controller.start(stage);		
	
					Scene scene = new Scene(myPane);
				    stage.setScene(scene);
				    prevStage.close();
				    stage.setResizable(false);
				    stage.show();
				    stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				        private File usernameFile;

						public void handle(WindowEvent we) {
				        	  usernameFile = new File("userList.ser");
		  		    		  try {
		  		    			  FileOutputStream f_out = new FileOutputStream("userList.ser");
		  		    			  obj_out = new ObjectOutputStream(f_out);
		  		    			  obj_out.writeObject(userList);
		  		    			  f_out.close();
		  		    			  obj_out.close();
		  		    		  } catch (IOException e) {
		  		    		  }
				        	  Stage stage = new Stage();
				  		      Pane myPane = null;
				  			  FXMLLoader loader = new FXMLLoader(getClass().getResource("Login.fxml"));
				  			  
							  try {
								myPane = (Pane) loader.load();
							  } catch (IOException e) {
							  }
				  			  
				  			  LoginController controller = (LoginController) loader.getController();
				  			  controller.setPrevStage(stage);
				  			  
	    					  try {
								controller.start(stage);
	    					  } catch (ClassNotFoundException | IOException e) {
	    					  }
	    					  
				  		 	  Scene scene = new Scene(myPane);
				  		      stage.setScene(scene);
				  		      prevStage.close();
				  			  stage.setResizable(false);
				  		      stage.show();
				  		      stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
				  		    	  private File usernameFile;

				  		    	  public void handle(WindowEvent we) {
				  		    		  usernameFile = new File("userList.ser");
				  		    		  try {
				  		    			  FileOutputStream f_out = new FileOutputStream("userList.ser");
				  		    			  obj_out = new ObjectOutputStream(f_out);
				  		    			  obj_out.writeObject(userList);
				  		    			  f_out.close();
				  		    			  obj_out.close();
				  		    		  } catch (IOException e) {
				  		    		  }
				  		    	  }
				  		      	}); 
				          }
				    });
			    }
		    }
		}
	}
}