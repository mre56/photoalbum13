package view;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Optional;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Pair;

/**
 * Controller for the tag editor scene. This scene is used to manipulate the
 * tags of a single photo, by adding and removing specific tags. The interface
 * looks almost identical to the admin interface and has very similar
 * functionality.
 *
 * @author Alex Weinrich
 * @author Matt Eder
 */
public class TagEditorController {

    /**
     * FXML reference for the add button to add tags to a photo
     */
    @FXML
    Button addButton;
    /**
     * FXML reference for the delete button to remove tags from a photo
     */
    @FXML
    Button deleteButton;
    /**
     * FXML reference for the list of all tag pairs that exist for a photo
     */
    @FXML
    ListView<String> tagsListView;
    /**
     * The current photo who's tags are being edited
     */
    Photo thisPhoto;
    /**
     * Arraylist of all tags for the photo
     */
    ArrayList<Pair<String, String>> tags;
    /**
     * List of all users that exist
     */
    ArrayList<User> userList;
    /**
     * Observable list used to place tags in the list view.
     */
    ObservableList<String> otags = FXCollections.observableArrayList();
    /**
     * Input stream used to read in users from the user file
     */
    private ObjectInputStream obj_in;
    /**
     * The current user who is editing the tags
     */
    User thisUser;
    /**
     * The current album that the photo was selected from
     */
    Album thisAlbum;

    /**
     * Start that initializes the tag editor by filling the observable list,
     * then the list view
     *
     * @param mainStage stage that the list view exists on
     */
    public void start(Stage mainStage) {
        for (Pair<String, String> im : tags) {
            otags.add(im.getKey() + " : " + im.getValue());
        }
        tagsListView.setItems(otags);
    }

    /**
     * Reads in all necessary information for the tag editor
     *
     * @param inport the photo who's tags are being changed
     * @param inuser the user who is making the tag changes
     * @param inalbum the album that the photo was selected from
     */
    public void getInfo(Photo inport, User inuser, Album inalbum) {
        thisUser = inuser;
        thisPhoto = inport;
        thisAlbum = inalbum;
        tags = inport.tags;
    }

    /**
     * Button handler for the add button. Produces a dialog box that is used to
     * add tag pairs to the photo. Only allows the user to click OK if a valid
     * tag pair is provided. If so, the pair is added to the photo and then
     * serialized to the user account. The list is finally updated to show all
     * current tags.
     *
     * @param e ActionEvent that caused the button to be pressed
     * @throws FileNotFoundException catches invalid file calls
     * @throws IOException for I/O expression errors
     * @throws ClassNotFoundException in case of invalid class calls
     */
    public void handleAddButton(ActionEvent e) throws FileNotFoundException, IOException, ClassNotFoundException {
        // Create the custom dialog.
        Dialog<Pair<String, String>> dialog = new Dialog<>();
        dialog.setTitle("New Tag Dialog");
        dialog.setHeaderText("Enter the New Tag");

        // Set the button types.
        ButtonType loginButtonType = new ButtonType("OK", ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        // Create the username and password labels and fields.
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField type = new TextField();
        type.setPromptText("Type");
        TextField value = new TextField();
        value.setPromptText("Value");

        grid.add(new Label("Type:"), 0, 0);
        grid.add(type, 1, 0);
        grid.add(new Label("Value:"), 0, 1);
        grid.add(value, 1, 1);

        // Enable/Disable login button depending on whether a username was entered.
        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        loginButton.setDisable(true);

        // Do some validation (using the Java 8 lambda syntax).
        value.textProperty().addListener((observable, oldValue, newValue) -> {
            loginButton.setDisable(newValue.trim().isEmpty());
            loginButton.setDisable(true);
            boolean validTag = true;
            for (Pair<String, String> a : tags) {
                if (a.getKey().equalsIgnoreCase(type.getText()) && a.getValue().equalsIgnoreCase(newValue)) {
                    validTag = false;
                }
            }
            if (validTag) {
                loginButton.setDisable(false);
            }
        });

        dialog.getDialogPane().setContent(grid);

        // Request focus on the username field by default.
        Platform.runLater(() -> type.requestFocus());

        // Convert the result to a username-password-pair when the login button is clicked.
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Pair<>(type.getText(), value.getText());
            }
            return null;
        });

        Optional<Pair<String, String>> result = dialog.showAndWait();

        if (result.isPresent()) {
            Pair<String, String> newPair = new Pair(result.get().getKey(), result.get().getValue());
            thisPhoto.addTag(newPair);
            tags = thisPhoto.tags;
            otags.add(newPair.getKey() + " : " + newPair.getValue());
            tagsListView.setItems(otags);
            //this.updateUserList();
        }

    }

    /**
     * Handler for the delete button. Produces a dialog box asking for
     * confirmation from the user to delete the selected tag pair in the
     * listview if one is selected. Once confirmed, the list pair is deleted
     * from the photo and saved to the serialized file.
     *
     * @param e ActionEvent that caused the button to be pressed
     * @throws IOException for I/O expression errors
     * @throws ClassNotFoundException in case of invalid class calls
     */
    public void handleDeleteButton(ActionEvent e) throws IOException, ClassNotFoundException {
        if (!tagsListView.getSelectionModel().isEmpty()) {
            //Delete Album(or anything) Dialog

            Alert deleteAlert = new Alert(Alert.AlertType.CONFIRMATION);
            deleteAlert.setTitle("Confirmation Dialog");
            deleteAlert.setHeaderText("Delete Tag " + tagsListView.getSelectionModel().getSelectedItem());
            deleteAlert.setContentText("Are you sure?");

            Optional<ButtonType> result = deleteAlert.showAndWait();
            if (result.get() == ButtonType.OK) {
                Pair<String, String> Removed = tags.remove(tagsListView.getSelectionModel().getSelectedIndex());
                int i = 0;
                for (Pair<String, String> u : tags) {
                    if (Removed.equals(u)) {
                        tags.remove(i);
                        break;
                    }
                    i++;
                }

                otags.remove(tagsListView.getSelectionModel().getSelectedIndex());
                tagsListView.setItems(otags);
                thisPhoto.tags = tags;
                //this.updateUserList();
            }
        }
    }
}
